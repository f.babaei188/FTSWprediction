# -*- coding: utf-8 -*-
"""
Created on Thu May 19 09:55:14 2022

@author: Fahimeh
"""

import tensorflow as tf
from tensorflow.keras import layers
import sys
base = 'C:/Users/Asus/.spyder-py3/gitlab'
sys.path.append(base)
import hyperparameters as param

hidden_units=param.hidden_units
dropout_size=param.dropout_size
lr=param.lr
decay=param.decay

def R_squared(y, y_pred):
  residual = tf.reduce_sum(tf.square(tf.subtract(y, y_pred)))
  total = tf.reduce_sum(tf.square(tf.subtract(y, tf.reduce_mean(y))))
  r2 = tf.subtract(1.0, tf.math.truediv(residual, total))
  return r2
def creat_Lstm(X_train):
    input1=tf.keras.layers.Input(shape=(X_train.shape[1],X_train.shape[2]))
    x=tf.keras.layers.Bidirectional(layers.LSTM(hidden_units, return_sequences=True, activation='relu'))(input1)
    #,recurrent_dropout=dropout_size, 
                                     
                                         #,name='lstm1')))(input1)
   # x=layers.Activation(hard_swish)(x)
    x=layers.Dropout(dropout_size)(x)
    x=tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(hidden_units,#,recurrent_dropout=dropout_size, 
                                      return_sequences=False,activation='relu'))(x)#)))(dropout)
   # x=layers.Activation(hard_swish)(x)
    x=layers.Dropout(dropout_size)(x)
   # danse=layers.Dense(256, activation='relu')(dropout)
   # dropout=layers.Dropout(dropout_size)(danse)
    output =tf.keras.layers.Dense(n_out)(x)
    
    model =tf.keras.models.Model(inputs=input1, outputs=output)
    opt = tf.keras.optimizers.Adam(learning_rate=lr,decay=decay)
    model.compile(optimizer=opt, loss='mse', metrics=["RootMeanSquaredError", R_squared])
    return model
   
