# -*- coding: utf-8 -*-
"""
Created on Thu May 19 10:03:46 2022

@author: Fahimeh
"""
import numpy as np
from collections import deque
from sklearn.preprocessing import MinMaxScaler,StandardScaler
import pandas as pd

import sys
#base = 'C:/Users/Asus/.spyder-py3/gitlab'
#sys.path.append(base)

import hyperparameters as param

def data_preprocessing(df):
    #normilized the data
    values=df.values
    scaler=MinMaxScaler()#StandardScaler()###
    
    values_normal=scaler.fit_transform(values)
   
  
    df=pd.DataFrame(values_normal, columns= df.columns, index=df.index)
    return df,scaler








def data_split_one_day_ahead(df):    
    sequential_data=[]
    prev_day = deque(maxlen=param.seq_len)
    
    for i in df.values:
        prev_day.append([n for n in i[:-param.n_out]])
        
        if len(prev_day)==param.seq_len:
            sequential_data.append([np.array(prev_day),i[-param.n_out:]])
            
         
    X=[]             
    Y=[]
    
    for seq , target in sequential_data:
        X.append(seq)
        Y.append(target)
    return np.array(X), np.array(Y)   



def data_split_7_days_prediction(df):    
    sequential_data=[]
    prev_day = deque(maxlen=param.seq_len)
    y=deque(maxlen=param.seq_len)
    
    for i in df.values:
        prev_day.append([n for n in i[:-param.n_out]])
        y.append(i[-param.n_out:])
        
        if len(prev_day)==param.seq_len:
            sequential_data.append([np.array(prev_day),np.array(y)])
            
         
    X=[]             
    Y=[]
    
    for seq , target in sequential_data:
        X.append(seq)
        Y.append(target)
    return np.array(X), np.array(Y)   

