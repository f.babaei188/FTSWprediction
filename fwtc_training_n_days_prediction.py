# -*- coding: utf-8 -*-
"""
Created on Wed May 11 16:07:16 2022

@author: Fahimeh
"""



import pandas as pd

import numpy as np
import os
from collections import deque

from sklearn.utils import shuffle

from sklearn.metrics import mean_squared_error

import matplotlib.pyplot as plt
import time
from sklearn.model_selection import train_test_split
import tensorflow as tf

from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

import sys
#base = 'C:/Users/Asus/.spyder-py3/gitlab'
#sys.path.append(base)

import LSTM_model as model
import hyperparameters as param
import preprocessing as pro
from pickle import dump
 
NAME = f"swc1_diario_biodagro-{int(time.time())}.h5"







def R_squared(y, y_pred):
  residual = tf.reduce_sum(tf.square(tf.subtract(y, y_pred)))
  total = tf.reduce_sum(tf.square(tf.subtract(y, tf.reduce_mean(y))))
  r2 = tf.subtract(1.0, tf.math.truediv(residual, total))
  return r2





dir_='D:/SM_estimation_paper'

df= pd.read_csv(os.path.join(dir_,'00203CEE_station_data_plot1_revers.csv'), encoding= 'unicode_escape', index_col='time')

df.dropna(inplace=True)
df.drop([ 'tem(avg)', 'tem(max)','dev_point (avg)',
       'dev_poin(Minuto)', 'SR (avg)', 'VPD (avg)',  'RH (max)','LW (time)','VS (max)', 'VS (max).1','SM (20cm)', 'SM (40cm)',
       'SM (60cm)', 'SM (80cm)','summand' , 'WS (avg)', 'RH (avg)', 
       'RH (Minuto)'

       
      ], axis=1,inplace=True)

df.dropna(inplace=True)
 
   
#split the data into the input of the model and true value of the yield
X,Y=pro.data_split_7_days_prediction(df)

indices = np.arange(len(X))
X_train, x_test, y_train, y_test,ind1,ind2 = train_test_split(X, Y,indices, test_size=0.2, random_state=42)


X_train, y_train = shuffle(X_train, y_train )

data=np.concatenate((X_train,y_train),axis=2)
data1=np.reshape(data,(data.shape[0]*data.shape[1],data.shape[2]))
data1=pd.DataFrame(data1)

#normalize the training set in order to use for training process
data1, scaler=pro.data_preprocessing(data1)
dump(scaler, open('scaler_7days.pkl', 'wb'))

#Again reshape the normalized data into the appropriate shape for input and output of the model

data=np.reshape(data1.values,(data.shape[0],data.shape[1],data.shape[2]))
X_train_minmax=data[:,:,:-param.n_out]
y_train=data[:,:,-param.n_out] 
X_train=X_train_minmax






model=model. creat_Lstm(X_train)
model.summary()
tensorboard=tf.keras.callbacks.TensorBoard(
    log_dir='D:/SM_estimation_paper/logs')
checkpoint = tf.keras.callbacks.ModelCheckpoint(NAME, monitor='val_loss', verbose=1, save_best_only=True, mode='min')
Early=tf.keras.callbacks.EarlyStopping(
    monitor="val_loss",
    min_delta=0,
    patience=10,
    verbose=1,
    mode="min",
    baseline=None,
    restore_best_weights=True,
)
#model.load_weights('swc1_diario_biodagro-1652178100.h5')#('swc1_diario_biodagro-1649866152.h5')

history=model.fit(X_train,y_train,batch_size=param.Batch_size,
                         epochs=param.Epoch, 
                         #validation_data=(x_val,y_val),
                         validation_split=0.2,
                         verbose=2, 
                         callbacks=[Early,checkpoint,tensorboard]) 



data1=np.concatenate((x_test,y_test),axis=2)

data2=np.reshape(data1,(data1.shape[0]*data1.shape[1],data1.shape[2]))
#convert the test set into a number between 0 and 1 using the Min_Max scaler from the training set
data1_re=scaler.transform(data2)
data1_re=np.reshape(data1_re,(data1.shape[0],data1.shape[1],data1.shape[2]))
x_test1=data1_re[:,:,:-param.n_out]
y_test1=data1_re[:,:,-param.n_out] 
#reshape the test set into the appropriate shape for prediction


#prediction using trained model
yhat = model.predict(x_test1)


#reshape and concatenate  the x_test and prediction  to denormalize the prediction

data3=np.concatenate((x_test,yhat),axis=2)
data3=np.reshape(data3,(data1.shape[0]*data1.shape[1],data1.shape[2]))
#denormalize the prediction and true value
data3=scaler.inverse_transform(data3)
data3=np.reshape(data3,(data1.shape[0],data1.shape[1],data1.shape[2]))
inv_yhat=data3[:,:,-param.n_out]
    
#inv_yhat=np.expand_dims(inv_yhat, axis=2)
y_test=np.reshape(y_test,(y_test.shape[0],y_test.shape[1]*y_test.shape[2]))

#r2_squer and mean squared error between the true values and the prediction values by model
r=R_squared(y_test,inv_yhat)
print(f'R_square={r}')       



print(np.sqrt(mean_squared_error(y_test,inv_yhat)))



fig, ax = plt.subplots(figsize=(20, 10))

# Add x-axis and y-axis.
ax.plot(y_test[3],
           color='purple', label='ilha1')
ax.plot(
         inv_yhat[3],
           label='ilha1')



fig, ax = plt.subplots(figsize=(5, 4))

ax.scatter( y_test,inv_yhat, label='ilha1')  # Plot some data on the axes.
ax.plot(y_test, y_test, label='ilha2', color='yellow', linestyle="--")  # Plot more data on the axes...

ax.set_xlabel('True value')  # Add an x-label to the axes.
ax.set_ylabel('Predicted value')  # Add a y-label to the axes.
ax.set_title(f"Parcela 46-FTSW ")  # Add a title to the axes.
#ax.legend();  # Add a legend.
#plt.savefig(f'D:/SM_estimation_paper/sm_plots/ftsw_ilha1_VPDmin_RHmin_pre_ETo_Standars.jpg' ,dpi=500)


