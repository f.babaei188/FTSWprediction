# -*- coding: utf-8 -*-
"""
Created on Thu May 19 10:07:54 2022

@author: Fahimeh
"""





#ET parameters: Batch_size=63  |dropout_size=0.09313|lr= 0.00089| decay= 0.09313
#FWST parameters:  40.86    |  0.09313  |  0.003456 |  0.003968 |  158.6  


seq_len=7 #(number of days for predicting ET)
Batch_size=63 # number of batch during training

Epoch=500 #number of training step
lr= 0.00089 #learning rate 
decay= 0.00094 #learning rate decay
hidden_units=38 #number of lstm unit in each lstm layer
dropout_size=0.09313 
n_out=1 


