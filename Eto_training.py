

import pandas as pd

import numpy as np
import os
from sklearn.utils import shuffle
from sklearn.metrics import mean_squared_error



import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler,StandardScaler
import time
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import tensorflow as tf
from pickle import dump


import sys
yamnet_base = 'C:/Users/Asus/.spyder-py3/gitlab'
sys.path.append(yamnet_base)

import LSTM_model as model
import preprocessing as pro
import hyperparameters as param






def R_squared(y, y_pred):
  residual = tf.reduce_sum(tf.square(tf.subtract(y, y_pred)))
  total = tf.reduce_sum(tf.square(tf.subtract(y, tf.reduce_mean(y))))
  r2 = tf.subtract(1.0, tf.math.truediv(residual, total))
  return r2


NAME = f"eto_diario_biodagro-{int(time.time())}.h5"

dir_='D:/SM_estimation_paper'

df= pd.read_csv(os.path.join(dir_,'Etotrain.csv'), encoding= 'unicode_escape', index_col='time')

df.dropna(inplace=True)
df.drop([ 'dev_point (avg)',
       'dev_poin(Minuto)', 'SR (avg)', 'VPD (avg)', 'VPD (Minuto)','LW (time)','VS (max)', 'VS (max).1','SM (20cm)', 'SM (40cm)',
       'SM (60cm)', 'SM (80cm)','summand' , 'WS (avg)','Precipitation','FTSW',
       
       
      ], axis=1,inplace=True)

df.dropna(inplace=True)



   
#split the data into the input of the model and true value of the yield

X_train,y_train=pro.data_split(df)



X_train, y_train = shuffle(X_train, y_train )

# reshape the X_train and Y_train and concatenate them in order to have the appropriate shape for the data preprocessing function
X_train_reshape=np.reshape(X_train,(X_train.shape[0]*X_train.shape[1],X_train.shape[2]))
#y_train=y_train.reshape(-1, 1)
m=np.zeros((X_train.shape[0]*X_train.shape[1]-len(y_train),1))
Y_train_reshape=np.concatenate((y_train,m),axis=0)
                                
y_train_reshape=Y_train_reshape.reshape(-1, 1)
data=np.concatenate((X_train_reshape,y_train_reshape),axis=1)
data=pd.DataFrame(data)

#normalize the training set in order to use for training process
data, scaler=pro.data_preprocessing(data)
dump(scaler, open('scaler_eto.pkl', 'wb'))

#Again reshape the normalized data into the appropriate shape for input and output of the model
X_train_minmax=data.values[:,:-param.n_out]
y_train=data.values[:len(y_train),-param.n_out] 
X_train=np.reshape(X_train_minmax,(X_train.shape[0],X_train.shape[1],X_train.shape[2]))

#X_train, x_val, y_train, y_val = train_test_split(X_train, y_train, test_size=0.1, random_state=42)





model=model.creat_Lstm(X_train)
model.summary()
tensorboard=tf.keras.callbacks.TensorBoard(
    log_dir='D:/SM_estimation_paper/logs')
checkpoint = tf.keras.callbacks.ModelCheckpoint(NAME, monitor='val_loss', verbose=1, save_best_only=True, mode='min')
Early=tf.keras.callbacks.EarlyStopping(
    monitor="val_loss",
    min_delta=0,
    patience=10,
    verbose=1,
    mode="min",
    baseline=None,
    restore_best_weights=True,
)
#model.load_weights('swc1_diario_biodagro-1652106970.h5')#('swc1_diario_biodagro-1649866152.h5')
history=model.fit(X_train,y_train,batch_size=param.Batch_size,
                         epochs=param.Epoch, 
                         #validation_data=(x_val,y_val),
                         validation_split=0.2,
                         verbose=2, 
                         callbacks=[Early,checkpoint,tensorboard]) 





df_test= pd.read_csv(os.path.join(dir_,'ETOtestset.csv'), index_col='time')

df_test.dropna(inplace=True)
df_test.drop([ 'Precipitation', 'LW (time)', 'WS (avg)', 'VS (max)',
'VS (max).1','ETo calculator'
        ], axis=1,inplace=True)#,'RH (max)', 'RH (Minuto)', 'tem(max)', 'tem (Minuto)',
       #'Daily ET0 [mm]' ,'VPD (Minuto)',  'VPD (avg)', 'RH (avg)',], axis=1,inplace=True)


df.dropna(inplace=True)

x_test,y_test=pro.data_split(df_test)
x_test_reshape=np.reshape(x_test,(x_test.shape[0]*x_test.shape[1],x_test.shape[2]))



m=np.zeros((x_test.shape[0]*x_test.shape[1]-len(y_test),1))
y_test_reshape=np.concatenate((y_test,m),axis=0).reshape(-1, 1)
data1=np.concatenate((x_test_reshape,y_test_reshape),axis=1)

data1=pd.DataFrame(data1)
#convert the test set into a number between 0 and 1 using the Min_Max scaler from the training set
data1_re=scaler.transform(data1)

x_test1=data1_re[:,:-param.n_out]
y_test1=data1_re[:len(y_test),-param.n_out] 
#reshape the test set into the appropriate shape for prediction
x_test1=np.reshape(x_test1,(x_test.shape[0],x_test.shape[1],x_test.shape[2]))

#prediction using trained model
yhat = model.predict(x_test1)


#reshape and concatenate  the x_test and prediction  to denormalize the prediction
yhat_reshape=np.concatenate((yhat,m),axis=0)
data2=np.concatenate((x_test_reshape,yhat_reshape),axis=1)
#denormalize the prediction and true value
data1=scaler.inverse_transform(data1_re)
data2=scaler.inverse_transform(data2)
inv_y=data1[:len(y_test),-param.n_out]
inv_yhat=data2[:len(y_test),-param.n_out]
    



#r2_squer and mean squared error between the true values and the prediction values by model
r=R_squared(inv_y,inv_yhat)
print(f'R_square={r}')       



print(np.sqrt(mean_squared_error(inv_y,inv_yhat)))




fig, ax = plt.subplots(figsize=(20, 10))

# Add x-axis and y-axis.
ax.plot(inv_y,
           color='purple', label='ilha1')
ax.plot(
         inv_yhat,
           label='ilha1')



fig, ax = plt.subplots(figsize=(5, 4))

ax.scatter( inv_y,inv_yhat, label='ilha1')  # Plot some data on the axes.
ax.plot(inv_y, inv_y, label='ilha2', color='yellow', linestyle="--")  # Plot more data on the axes...

ax.set_xlabel('True value')  # Add an x-label to the axes.
ax.set_ylabel('Predicted value')  # Add a y-label to the axes.
ax.set_title(f"ETo ")