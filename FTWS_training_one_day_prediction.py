# -*- coding: utf-8 -*-
"""
Created on Fri Mar 18 11:49:50 2022

@author: Fahimeh
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Feb 22 14:03:27 2022

@author: Fahimeh
"""
#{'target': -0.21576160192489624, 'params': {'Batch_size': 34.138582500235756, 'Droup_out_size': 0.3240207630881275, 'decay': 0.009208682153565323, 'lr': 0.00664778636072961}}

#1. Variance inflation factor (VIF)

import pandas as pd

import numpy as np
import os
from collections import deque

from sklearn.utils import shuffle

from sklearn.metrics import mean_squared_error

import pandas as pd

import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler,StandardScaler
import time
from sklearn.model_selection import train_test_split
import tensorflow as tf
from tensorflow.keras import layers
import seaborn as sns
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
from pickle import dump


import sys
yamnet_base = 'C:/Users/Asus/.spyder-py3/gitlab'
sys.path.append(yamnet_base)
import LSTM_model as model
import hyperparameters as param
import preprocessing as pro
  






# A function to split the data for supervised learning


#define R-square metric for evaluation of the model
def R_squared(y, y_pred):
  residual = tf.reduce_sum(tf.square(tf.subtract(y, y_pred)))
  total = tf.reduce_sum(tf.square(tf.subtract(y, tf.reduce_mean(y))))
  r2 = tf.subtract(1.0, tf.math.truediv(residual, total))
  return r2




#choose a name to save the model
NAME = f"swc1_diario_biodagro-{int(time.time())}.h5"


#open the csv file contains the data
dir_='D:/SM_estimation_paper'

df= pd.read_csv(os.path.join(dir_,'00203CEE_station_data_plot1_revers.csv'), encoding= 'unicode_escape', index_col='time')

#drop the columns we do not need them
df.dropna(inplace=True)
df.drop([ 'tem(avg)', 'tem(max)','dev_point (avg)',
       'dev_poin(Minuto)', 'SR (avg)', 'VPD (avg)',  'RH (max)','LW (time)','VS (max)', 'VS (max).1','SM (20cm)', 'SM (40cm)',
       'SM (60cm)', 'SM (80cm)','summand' , 'WS (avg)', 'tem (Minuto)', 'VPD (Minuto)',
       'RH (Minuto)','day'

       
      ], axis=1,inplace=True)

df.dropna(inplace=True)


   
#split the data into the input of the model and true value of FWST
X,Y=pro.data_split_one_day_ahead(df)

# divide dataset into training and test set
indices = np.arange(len(X))
X_train, x_test, y_train, y_test,ind1,ind2 = train_test_split(X, Y,indices, test_size=0.2, random_state=42)


# shuffle the training set 
X_train, y_train = shuffle(X_train, y_train )

#reshape X_train and y_train in order to normalize them 
X_train_reshape=np.reshape(X_train,(X_train.shape[0]*X_train.shape[1],X_train.shape[2]))
m=np.zeros((X_train.shape[0]*X_train.shape[1]-len(y_train),1))
Y_train_reshape=np.concatenate((y_train,m),axis=0)
                                
y_train_reshape=Y_train_reshape.reshape(-1, 1)

data=np.concatenate((X_train_reshape,y_train_reshape),axis=1)

data=pd.DataFrame(data)

#normalize the training set in order to use for training process
data, scaler=pro.data_preprocessing(data)
# save the scaler to use in the future prediction
dump(scaler, open('scaler_1day.pkl', 'wb'))

#Again reshape the normalized data into the appropriate shape for input and output of the model
X_train_minmax=data.values[:,:-param.n_out]
y_train=data.values[:len(y_train),-param.n_out] 
X_train=np.reshape(X_train_minmax,(X_train.shape[0],X_train.shape[1],X_train.shape[2]))





#creat the model
model=model.creat_Lstm(X_train)
model.summary()
# using tensorboard,checkpoint , EarlyStopping as callbacks of the model
tensorboard=tf.keras.callbacks.TensorBoard(
    log_dir='D:/SM_estimation_paper/logs')
checkpoint = tf.keras.callbacks.ModelCheckpoint(NAME, monitor='val_loss', verbose=1, save_best_only=True, mode='min')
Early=tf.keras.callbacks.EarlyStopping(
    monitor="val_loss",
    min_delta=0,
    patience=10,
    verbose=1,
    mode="min",
    baseline=None,
    restore_best_weights=True,
)
#model.load_weights('swc1_diario_biodagro-1652176316.h5', by_name='True')#('swc1_diario_biodagro-1649866152.h5')

#train the model
history=model.fit(X_train,y_train,batch_size=param.Batch_size,
                         epochs=param.Epoch, 
                         #validation_data=(x_val,y_val),
                         validation_split=0.2,
                         verbose=2, 
                         callbacks=[Early,checkpoint,tensorboard]) 


#reshape the X_test and Y_test inorder to normalize them using scaler from training

x_test_reshape=np.reshape(x_test,(x_test.shape[0]*x_test.shape[1],x_test.shape[2]))



m=np.zeros((x_test.shape[0]*x_test.shape[1]-len(y_test),1))
y_test_reshape=np.concatenate((y_test,m),axis=0).reshape(-1, 1)
data1=np.concatenate((x_test_reshape,y_test_reshape),axis=1)

data1=pd.DataFrame(data1)
#convert the test set into a number between 0 and 1 using the Min_Max scaler from the training set
data1_re=scaler.transform(data1)

x_test1=data1_re[:,:-param.n_out]
y_test1=data1_re[:len(y_test),-param.n_out] 
#reshape the test set into the appropriate shape for prediction
x_test1=np.reshape(x_test1,(x_test.shape[0],x_test.shape[1],x_test.shape[2]))

#prediction using trained model
yhat = model.predict(x_test1)


#reshape and concatenate  the x_test and prediction  to denormalize the prediction
yhat_reshape=np.concatenate((yhat,m),axis=0)
data2=np.concatenate((x_test_reshape,yhat_reshape),axis=1)
#denormalize the prediction and true value
data1=scaler.inverse_transform(data1_re)
data2=scaler.inverse_transform(data2)
inv_y=data1[:len(y_test),-param.n_out]
inv_yhat=data2[:len(y_test),-param.n_out]
    



#r2_squer and mean squared error between the true values and the prediction values by model
r=R_squared(inv_y,inv_yhat)
print(f'R_square={r}')       



print(np.sqrt(mean_squared_error(inv_y,inv_yhat)))





#draw true values vs. predicted values by model
fig, ax = plt.subplots(figsize=(5, 4))

ax.scatter( inv_y,inv_yhat, label='ilha1')  # Plot some data on the axes.
ax.plot(inv_y, inv_y, label='ilha2', color='yellow', linestyle="--")  # Plot more data on the axes...

ax.set_xlabel('True value')  # Add an x-label to the axes.
ax.set_ylabel('Predicted value')  # Add a y-label to the axes.
ax.set_title(f"Parcela 46-FTSW ")  # Add a title to the axes.
#ax.legend();  # Add a legend.
#plt.savefig(f'D:/SM_estimation_paper/sm_plots/ftsw_ilha1_VPDmin_RHmin_pre_ETo_Standars.jpg' ,dpi=500)



