# -*- coding: utf-8 -*-
"""
Created on Wed May 11 16:48:56 2022

@author: Fahimeh
"""

# -*- coding: utf-8 -*-
"""
Created on Wed May  4 10:56:07 2022

@author: Fahimeh
"""
import numpy as np
from sklearn.metrics import mean_squared_error
import pandas as pd
import matplotlib.pyplot as plt
import tensorflow as tf
from pandas.plotting import register_matplotlib_converters
from collections import deque
from pickle import load

register_matplotlib_converters()


def R_squared(y, y_pred):
  residual = tf.reduce_sum(tf.square(tf.subtract(y, y_pred)))
  total = tf.reduce_sum(tf.square(tf.subtract(y, tf.reduce_mean(y))))
  r2 = tf.subtract(1.0, tf.math.truediv(residual, total))
  return r2


n_out=1
seq_len=7
dir_='D:/SM_estimation_paper/2003_mean.csv'
df=pd.read_csv(dir_, encoding= 'unicode_escape')
df.drop([ 'time',
        'Temp max ', 'Temp med ', 'Gelo (h)', 'H.R. min',
              'H.R. max ','H.R. >90 (h)', 'H.R. 80-90 (h)',
              'H.R. <40 (h)','Precip max (mm)', 'Humect','Temp -10cm ',
              'eto','Temp min ', 
      ], axis=1,inplace=True)



def data_split(df):    
    sequential_data=[]
    prev_day = deque(maxlen=seq_len)
    
    for i in df.values:
        prev_day.append([n for n in i[:-n_out]])
        
        if len(prev_day)==seq_len:
            sequential_data.append([np.array(prev_day),i[-n_out:]])
            
 
  #  random.shuffle(sequential_data)
        
    X=[]             
    Y=[]
    
    for seq , target in sequential_data:
        X.append(seq)
        Y.append(target)
    return np.array(X), np.array(Y)   
 

X1,Y1=data_split(df)
X=[]
Y=[]
def min(a):
    return np.isnan(np.min(a))

for i , j in zip(X1,Y1):
    if min(j)!=True:
        print(j)
        X.append(i)
        Y.append(j)

X=np.array(X)
Y=np.array(Y)        
#X, Y= shuffle(X,Y )        
        

X_train_reshape=np.reshape(X,(X.shape[0]*X.shape[1],X.shape[2]))
#y_train=y_train.reshape(-1, 1)
m=np.zeros((X.shape[0]*X.shape[1]-len(Y),1))
Y_train_reshape=np.concatenate((Y,m),axis=0)
                                
y_train_reshape=Y_train_reshape.reshape(-1, 1)
data=np.concatenate((X_train_reshape,y_train_reshape),axis=1)
data=pd.DataFrame(data)

#normalize the training set in order to use for training process
scaler = load(open('scaler.pkl', 'rb'))

data=scaler.transform(data)
#data, scaler=data_preprocessing(data)

#Again reshape the normalized data into the appropriate shape for input and output of the model
X_train_minmax=data[:,:-n_out]
y=data[:len(Y),-n_out] 
x=np.reshape(X_train_minmax,(X.shape[0],X.shape[1],X.shape[2]))
    
# FWST: swc1_diario_biodagro-1651831879.h5'
model=tf.keras.models.load_model('swc1_diario_biodagro-1652176316.h5',  compile=False)

yhat = model.predict(x)

#reshape and concatenate  the x_test and prediction  to denormalize the prediction
yhat_reshape=np.concatenate((yhat,m),axis=0)
data2=np.concatenate((X_train_reshape,yhat_reshape),axis=1)
#denormalize the prediction and true value
data2=scaler.inverse_transform(data2)
inv_yhat=data2[:len(Y),-n_out]
    






y_mean=np.array([np.mean(Y) for i in range(len(Y))])
mean_error=((mean_squared_error(Y,y_mean)))

mse=((mean_squared_error(Y,inv_yhat)))

r2=1-(mse/mean_error)
print(r2)

print(np.sqrt(mean_squared_error(Y,inv_yhat)))



fig, ax = plt.subplots(figsize=(5, 4))

ax.scatter( Y,inv_yhat, label='ilha1')  # Plot some data on the axes.
ax.plot(Y, Y, label='ilha2', color='yellow', linestyle="--")  # Plot more data on the axes...

ax.set_xlabel('True value')  # Add an x-label to the axes.
ax.set_ylabel('Predicted value')  # Add a y-label to the axes.
ax.set_title(f"Alenquer, Lisboa (ETo from ETo calculator) ")  # Add a title to the axes.
#ax.legend();  # Add a legend.
#plt.savefig(f'D:/SM_estimation_paper/sm_plots/2003_2004_lisboa_rh-per_etocal.jpg' ,dpi=500)

